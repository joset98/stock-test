<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use HasFactory;

    protected $table = "payment_methods";

    protected $fillable = [ 
        'id',
        'tipo',
        'description',
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    /**
    * devuelve las compras que tengan asociada el tipo de pago.
    */
    public function purchases()
    {
        return $this->hasMany('App\Models\Purchase');
    }
}
