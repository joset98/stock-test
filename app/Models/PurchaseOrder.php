<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    use HasFactory;

    protected $table = 'purchase_orders';
    
    protected $fillable = [
        'id',
        'user_id',
        'total',
        'subtotal',
        'quantity_products',
        'status',
        'order_date',
    ]; 

    protected $hidden = [
        'create_at','update_at'
    ]; 

    /**
    * devuelve el usuario propietario de la orden de compra .
    */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'foreign_key');
    }

    /**
    * Devuelve la compra asociada a la orden de compra.
    */
   public function purchase()
   {
       return $this->hasOne('App\Models\Purchase', 'foreign_key');
   }

   public function listProducts()
   {
       return $this->hasMany('App\Models\ListProduct');
   }
   
}
