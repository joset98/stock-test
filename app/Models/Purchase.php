<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;

    protected $table = 'purchases';
    
    protected $fillable = [
        'id',
        'total',
        'sub_total',
        'description',
        'purchase_date',
        'payment_method_id',
        'setting_id',
        'purchase_order_id'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    
    /**
    * Devuelve el metodo de pago utilizado en una compra.
    */
    public function paymentMethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod','foreign_key');
    }

    
    /**
    * Devuelve la configuracion de compra que tenga asociada la compra.
    */
    public function setting()
    {
        return $this->belongsTo('App\Models\Setting','foreign_key');
    }

    /**
    * Devuelve la orden de compra asociada a la compra.
    */
   public function purchaseOrder()
   {
       return $this->belongsTo('App\Models\PurchaseOrder', 'foreign_key');
   }

}
