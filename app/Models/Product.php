<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = [
        'id',
        'name',
        'descripton',
        'price',
        'stock',
        'category_id'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    /**
    * devuelve la categoria a la que pertenece el producto.
    */
    public function category()
    {
        return $this->belongsTo('App\Models\Category','foreign_key');
    }

    public function listProducts()
    {
        return $this->hasMany('App\Models\ListProduct');
    }

    /**
    * Verifica si despues de la compra existen productos que deban estar fuera de stock .
    * y los almacena en el archivo
    */
    public static function storeItemtoFile ( $purchase , $min_stock )
    {
        $itemToFile = Product::select('products.id','products.name')
                            ->join('list_products', 'list_products.product_id', '=', 'products.id')
                            ->join('purchase_orders', 'purchase_orders.id', '=', 'list_products.purchase_order_id')
                            ->join('purchases', 'purchases.purchase_order_id', '=', 'purchase_orders.id')
                            ->where([
                                ['purchases.id',1],
                                ['products.stock', '<=' , $min_stock]
                                ])
                            ->get();
        
        if( ! empty( $itemToFile ) )  
            return 
                $itemToFile->map(function ($item){
                    return Storage::append('file.txt', $item->id ."-". $item->name);
                });
        return false;
    }

}
