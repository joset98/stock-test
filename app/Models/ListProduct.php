<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListProduct extends Model
{
    use HasFactory;

    protected $table = "list_products";

    protected $fillable = [ 
        'quantity_products',
        'total_product',
        'product_id',
        'purchase_order_id',
        'status'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    /**
    * devuelve la orden de compras que tengan asociada al listado de productos.
    */
    public function purchaseOrder()
    {
        return $this->belongsTo('App\Models\PurchaseOrder','foreign_key');
    }
 
    /**
    * devuelve los productos que tengan asociada al listado de productos.
    */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','foreign_key');
    }

}
