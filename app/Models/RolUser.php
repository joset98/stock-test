<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolUser extends Model
{
    use HasFactory;

    protected $table = "rol_users";

    protected $fillable = [ 
        'user_id',
        'rol_id',
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    public static function attachNewRol( $user , $rol )
    {
        $purchaseOrder = RolUser::create([
            'user_id' => $user,
            'rol_id' => $rol,
        ]);  
        
    }

}
