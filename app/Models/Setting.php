<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $table = 'settings';
    
    protected $fillable = [
        'id',
        'min_stock',
        'min_amount',
        'iva',
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    /**
    * devuelve las compras que tengan asociada una configuracion de compra.
    */
    public function purchases()
    {
        return $this->hasMany('App\Models\Purchase');
    }
}
