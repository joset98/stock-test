<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    
    protected $table = "categories";

    protected $fillable = [ 
        'id',
        'name',
        'description',
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    /**
    * Devuelve los productos pertenecientes a una categoria.
    */
    public function categories()
    {
        return $this->hasMany('App\Models\Product');
    }

    
}
