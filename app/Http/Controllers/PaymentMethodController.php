<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaymentMethod;

class PaymentMethodController extends Controller
{

    public function index()
    {
        $paymentMethod = PaymentMethod::all();
 
    	return response()->json([
    		'payment_method' => $paymentMethod,
        ],200);
    }

    public function store(Request $request)
    {
        $paymentMethod = PaymentMethod::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        if( ! $paymentMethod->save() ){
            return response()->json([
                'ErrorMessage' => 'Ocurrio un error al crear el metodo de pago',
            ],400);
        }

        return response()->json([
    		'payment_method' => $paymentMethod,
        ],200);
    }

    public function update(Request $request)
    {
        $paymentMethodUpdate = PaymentMethod::find($request->id);
        
        if( !$paymentMethod )
            return response()->json([
                'errorMessage' => 'Metodo de pago no encontrado'
            ],404);
        
        $paymentMethod->fill($request->all());
        $paymentMethod->save();
        return response()->json(['status' => 'ok', 
            'data' => $paymentMethod
        ], 200);
        
    }
    
    public function destroy(Request $request)
    {
        $paymentMethod = PaymentMethod::find($request->id);
        
        if(! $paymentMethod )
            return response()->json(['status' => 'notFound', 
                'message' => 'object not found'
            ], 404);
        
        $paymentMethod->delete();
        return response()->json(['status' => 'ok', 
            'data' => $paymentMethod
        ], 200);

    }
}
