<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RolUser;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();
 
    	return response()->json([
    		'users' => $users,
        ],200);
    }
       
    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        
        $user->save();
       
        return response()->json([
            'message' => 'usuario insertado',
        ],201);
    }

    public function update(Request $request)
    {
        $userUpdate = User::find($request->id);
        
        if( !$userUpdate )
            return response()->json([
                "errorMessage"=>"usuario no encontrado"
            ],404);
        
        $userUpdate->fill( 
        [   'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        $userUpdate->save();
        return response()->json(['status'=>'ok', 
            'data'=>$userUpdate
        ], 200);
        
    }
    
    public function destroy(Request $request)
    {
        $user = User::find($request->id);
        
        if(!$user)
            return response()->json(['status'=>'notFound', 
                'message'=>'object not found'
            ], 404);
        
        $user->delete();
        return response()->json(['status'=>'ok', 
            'data'=>$user
        ], 200);

    }

}
