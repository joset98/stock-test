<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PurchaseOrder;
use App\Models\ListProduct;

class ListProductController extends Controller
{
    public function addToOrder(Request $request)
    {
        $settings = DB::table('settings')->latest('created_at')->first(); 
        $product = DB::table('products')->where('id',$request->product_id)->first(); 
        $purchaseOrder = PurchaseOrder::where('id',$request->order_id)->first(); 
        $quantity_products = $request->quantity_products;
        $total_product = $quantity_products * $product->price ;

        if( ! ($product && $purchaseOrder) )
            return response()->json(['errorMessage' => 'la orden o el articulo no existe', 
            ], 404);

        if ( $product->stock <= $settings->min_stock )
            return response()->json(['errorMessage' => 'out of stock', 
                'settings' => $settings,
                'product' => $product
            ], 400);

        if( ! ( $listProduct = $this->isAddToOrder( $product, $purchaseOrder ) ) )
            $listProduct = ListProduct::create([
                'product_id' => $product->id,
                'purchase_order_id' => $purchaseOrder->id,
                'total_product' => $quantity_products * $product->price ,
                'quantity_products' => $request->quantity_products,
            ]);
        else {
            $listProduct->quantity_products += $quantity_products;
            $listProduct->total_product += $total_product;
        }

        $purchaseOrder->quantity_products += $quantity_products;
        $purchaseOrder->sub_total += $total_product;
        $purchaseOrder->total = $purchaseOrder->sub_total + ($purchaseOrder->sub_total * $settings->iva);
        $purchaseOrder->save();
        $listProduct->save();
        
        return response()->json(['status' => 'ok', 
            'data' => $purchaseOrder
        ], 200);
        
    }

    public function isAddToOrder( $product, $order )
    {
        return (
            ListProduct::where('product_id', $product->id) 
            ->where('purchase_order_id', $order->id )->first() 
        ); 

    }

}
