<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        // Guardamos en un arreglo los datos del usuario. 
        $user = array(
            'email' => $request->user,
            'password'=> $request->password
        );
        // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.

        if( Auth::attempt( $user ) )
        {
            $user = Auth()->user();
            Auth::login($user, true);
            $user->api_token = Str::random(60);
            $user->save();

            return response()->json([
                'message' => 'ha sido autenticado',
                'data' => $user,
            ],200);        

        }
        // En caso de que la autenticación haya fallado manda un mensaje al formulario de login y también regresamos los valores enviados con withInput().
        return response()->json([
            'ErrorMessage' => 'Ocurrio un error al efectuar el login',
        ],400);
    }

    public function logout()
    {
        $user = Auth::guard('api')->user();

        if ( $user ) {
            // Auth::logout();
            $user->api_token = null;
            $user->save();

            return response()->json([
                'message' => 'Thank you for using our application. User Logout '. $user->name,
            ], 201);
        }
        
        return response()->json([
            'error' => 'Unable to logout user',
            'code' => 401,
        ], 401);        
    }
}
