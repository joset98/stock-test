<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PurchaseOrder;
use App\Models\ListProduct;

class PurchaseOrderController extends Controller
{
    public function index()
    {
        $purchaseOrders = PurchaseOrder::all();
 
    	return response()->json([
    		'purchaseOrders' => $purchaseOrders,
        ],200);
    }

    public function store(Request $request)
    {
        $purchaseOrder = PurchaseOrder::create([
            'user_id' => $request->user_id,
            'order_date' => now(),
        ]);   

        if( ! $purchaseOrder->save() ){
            return response()->json([
                'ErrorMessage' => 'Ocurrio un error al crear la orden de compra',
            ],400);
        }

        return response()->json([
    		'purchaseOrder' => $purchaseOrder,
        ],200);
    }

    public function update(Request $request)
    {
        $purchaseOrderUpdate = PurchaseOrder::find( $request->id );
        
        if( !$purchaseOrderUpdate )
            return response()->json([
                'errorMessage' => 'orden de compra no encontrada'
            ],404);
        
        $purchaseOrderUpdate->fill( $request->all() );
        $purchaseOrderUpdate->save();
        return response()->json(['status' => 'ok', 
            'data' => $purchaseOrderUpdate
        ], 200);
        
    }
    
    public function destroy(Request $request)
    {
        $purchaseOrder = PurchaseOrder::find($request->id);
        
        if(!$purchaseOrder)
            return response()->json(['status' => 'notFound', 
                'message' => 'object not found'
            ], 404);
        $purchaseOrder->delete();

        return response()->json(['status'=>'ok', 
            'data' => $purchaseOrder
        ], 200);

    }

    public function showByUser(Request $request)
    {
        $purchaseOrderUser = DB::table('users')->where('id', $request->idUser)->get();

        if( !$purchaseOrderUser )
            return response()->json([
                'errorMessage' => 'Este usuario no ha realizado compras'
            ],404);
        

        return response()->json(['status' => 'ok', 
            'data' => $purchaseOrderUser
        ], 200);
        
    }
    
}
