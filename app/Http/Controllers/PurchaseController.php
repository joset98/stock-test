<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PurchaseOrder;
use App\Models\Purchase;
use App\Models\Product;
use Storage;


class PurchaseController extends Controller
{
    public function index()
    {
        $purchases = Purchase::all();
 
    	return response()->json([
    		'Purchases' => $purchases,
        ],200);
    }

    public function store(Request $request)
    {
        $settings = DB::table('settings')->latest('created_at')->first(); 
        $purchaseOrder = PurchaseOrder::where('id',$request->order_id)->first(); 

        if( $purchaseOrder ){

            if ( $purchaseOrder->sub_total <= $settings->min_amount )
                return response()->json(['errorMessage' => 'el total de la compra no alcanza el monto minimo requerido', 
                                        ], 404);
            
            // dd(Product::storeItemtoFile( 1 , $settings->min_stock ));
            
            $purchase = Purchase::create([
                'total' => $purchaseOrder->total,
                'sub_total' => $purchaseOrder->sub_total,
                'purchase_date' => now() ,
                'description' => $request->description,
                'setting_id' => $settings->id,
                'payment_method_id' => $request->payment_method,
                'purchase_order_id' => $purchaseOrder->id,
            ]);
            
            return response()->json(['status' => 'ok', 
                'data' => $purchase
            ], 200);
                
            if( ! $purchase->save() ){
                return response()->json([
                    'ErrorMessage' => 'Ocurrio un error al efectuar la compra',
                ],400);
            }
        
            return response()->json([
                'purchase' => $purchase,
            ],200);
        }

        return response()->json([
            'errorMessage' => 'la orden no existe', 
        ], 404);

    }
    
    public function update(Request $request)
    {
        $purchaseUpdate = Purchase::find( $request->id );
        
        if( !$purchaseUpdate )
            return response()->json([
                'errorMessage' => 'compra no encontrada'
            ],404);
        
        $purchaseUpdate->fill( $request->all() );
        $purchaseUpdate->save();
        return response()->json(['status' => 'ok', 
            'data' => $purchaseUpdate
        ], 200);
        
    }
    
    public function destroy(Request $request)
    {
        $purchase = Purchase::find($request->id);
        
        if(!$purchase)
            return response()->json(['status' => 'notFound', 
                'message' => 'object not found'
            ], 404);
        $purchase->delete();

        return response()->json(['status'=>'ok', 
            'data' => $purchase
        ], 200);
    }

    public function showByUser(Request $request)
    {
        $purchaseUser = DB::table('users')->where('id', $request->idUser)->get();

        if( !$purchaseUser )
            return response()->json([
                'errorMessage' => 'Este usuario no ha realizado compras'
            ],404);
        

        return response()->json(['status' => 'ok', 
            'data' => $purchaseUser
        ], 200);        
    }
}
