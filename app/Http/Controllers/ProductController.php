<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    
    public function index()
    {
        $products = Product::all();
 
    	return response()->json([
    		'products' => $products,
        ],200);
    }

    public function store(Request $request)
    {
        $product = Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'stock' => $request->stock,
            'category_id' => $request->category,
        ]);

        if( ! $product->save() ){
            return response()->json([
                'ErrorMessage' => 'Ocurrio un error al crear el usuario',
            ],400);
        }

        return response()->json([
    		'products' => $product,
        ],200);
    }

    public function update(Request $request)
    {
        $productUpdate = Product::find( $request->id );
        
        if( !$productUpdate )
            return response()->json([
                'errorMessage' => 'producto no encontrado'
            ],404);
        
        $productUpdate->fill( $request->all() );
        $productUpdate->save();
        return response()->json(['status' => 'ok', 
            'data' => $productUpdate
        ], 200);
        
    }
    
    public function destroy(Request $request)
    {
        $product = Product::find($request->id);
        
        if( !$Product )
            return response()->json(['status' => 'notFound', 
                'message' => 'object not found'
            ], 404);
        $product->delete();

        return response()->json(['status'=>'ok', 
            'data' => $product
        ], 200);

    }

    public function productsToUser()
    {
        $products = Product::all();
 
    	return response()->json([
    		'products' => $products,
        ],200);
    }
}
