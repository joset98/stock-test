<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;


class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::all();
 
    	return response()->json([
    		'setting' => $setting,
        ],200);
    }

    public function store(Request $request)
    {
        $setting = Setting::create([
            'min_stock' => $request->minStock,
            'min_amount' => $request->minAmount,
            'iva' => $request->iva,
        ]);

        if( ! $setting->save() ){
            return response()->json([
                'ErrorMessage' => 'Ocurrio un error al crear los ajustes',
            ],400);
        }

        return response()->json([
    		'setting' => $setting,
        ],200);
    }

    public function update(Request $request)
    {
        $settingUpdate = Setting::find($request->id);
        
        if( !$setting )
            return response()->json([
                'errorMessage' => 'ajustes no encontrados'
            ],404);
        
        $setting->fill($request->all());
        $setting->save();
        return response()->json(['status' => 'ok', 
            'data' => $setting
        ], 200);
        
    }
    
    public function destroy(Request $request)
    {
        $setting = Setting::find( $request->id );
        
        if(! $setting )
            return response()->json(['status' => 'notFound', 
                'message' => 'object not found'
            ], 404);
        
        $setting->delete();
        return response()->json(['status' => 'ok', 
            'data' => $Setting
        ], 200);

    }
}
