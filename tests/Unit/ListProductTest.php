<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;

class ListProductTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testToGenerateOrderByUser()
    {
        // $user = User::all()->random();
        // $response = $this->actingAs($user,'api')
        //                 ->postJson('/api/purchase-orders/create', [
        //                     'user_id' => $user->id,
        //                 ]);
                        // $response->dump();  
        // $response->assertStatus(200);   
    }

     public function testToAddProductToOrder()
    {
        $user = User::find(18);
        $response = $this->actingAs($user,'api')
                        ->postJson('/api/purchase-orders/add', [
                            'product_id' => 1,
                            'order_id' => 1,
                            'quantity_products' => 4
                        ]);
    }
}
