<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;

class LoginTest extends TestCase
{


    public function authenticateUser ($user)
    {        
        $request = [  
            'user' => $user->name,
            'password' => 'secret',
        ];

        return $response = $this->post('/api/login', $request);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testAuthenticatedToUser()
    {
        $user = User::all()->random();
        $response = $this->authenticateUser($user); 
        // $response->dump();

        $response->assertStatus(200);
        $this->assertAuthenticatedAs($user);
    }
    
    public function testLogout()
    {
        
        $user = User::all()->random();
        $response = $this->actingAs($user,'api')
                         ->post('/api/logout');

        // $response->dump();
        $response
        ->assertStatus(201);
    }
}
