<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol_users')->insert([
            'user_id' => 1,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);
        
        DB::table('rol_users')->insert([
            'user_id' => 2,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);
        
        DB::table('rol_users')->insert([
            'user_id' => 3,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);

        DB::table('rol_users')->insert([
            'user_id' => 4,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);

        DB::table('rol_users')->insert([
            'user_id' => 5,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);

        DB::table('rol_users')->insert([
            'user_id' => 6,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);

        DB::table('rol_users')->insert([
            'user_id' => 7,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);
        
        DB::table('rol_users')->insert([
            'user_id' => 8,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);

        DB::table('rol_users')->insert([
            'user_id' => 9,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);

        DB::table('rol_users')->insert([
            'user_id' => 10,
            'rol_id' => \App\Models\Rol::all()->random()->id
        ]);
    }
}
