<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'min_stock' => 5,
            'min_amount' => 120.99,
            'iva' => .12,
            'created_at' => now(),

        ]);
        
        DB::table('settings')->insert([
            'min_stock' => 7,
            'min_amount' => 125.00,
            'iva' => .16,
            'created_at' => now(),

        ]);
    }
}
