<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'cpu',
            'description' => 'unidad de control de procesamiento',
        ]);
        
        DB::table('categories')->insert([
            'name' => 'gpu',
            'description' => 'unidad de procesamiento grafico',
        ]);
        
        DB::table('categories')->insert([
            'name' => 'limpieza',
            'description' => 'productos de limpieza para el hogar',
        ]);

        DB::table('categories')->insert([
            'name' => 'higiene',
            'description' => 'productps de higiene personal',
        ]);
    }
}
