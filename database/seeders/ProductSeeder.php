<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // App\Room::all()->random()->id
        DB::table('products')->insert([
            'name' => Str::random(10),
            'price' => rand(10,100),
            'stock' => rand(10,100),
            'category_id' => \App\Models\Category::all()->random()->id,
        ]);

    }
}
