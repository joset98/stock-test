<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->double('total');
            $table->double('sub_total');
            $table->date('purchase_date');
            $table->text('description');
            $table->enum('status',['ACTIVO' , 'EN PROCESO', 'APROBADO'])->default('ACTIVO');
            $table->foreignId('payment_method_id')->constrained('payment_methods');
            $table->foreignId('setting_id')->constrained('settings');
            $table->foreignId('purchase_order_id')->constrained('purchase_orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
