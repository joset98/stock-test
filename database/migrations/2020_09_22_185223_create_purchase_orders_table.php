<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->id();            
            $table->double('total')->default(0.0);
            $table->double('sub_total')->default(0.0);
            $table->integer('quantity_products')->default(0);
            $table->enum('status',['ACTIVO' , 'EN PROCESO', 'APROBADO'])->default('ACTIVO');
            // $table->integer('status');
            $table->date('order_date');
            $table->foreignId('user_id')->constrained('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
