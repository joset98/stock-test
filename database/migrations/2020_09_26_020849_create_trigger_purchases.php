<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTriggerPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE TRIGGER update_product_stock AFTER INSERT ON purchases FOR EACH ROW
            BEGIN
                UPDATE products 
                    INNER JOIN list_products ON list_products.product_id = products.id 
                    INNER JOIN purchase_orders on purchase_orders.id = list_products.purchase_order_id 
                    INNER JOIN purchases on purchases.purchase_order_id = purchase_orders.id
                SET products.stock = products.stock - list_products.quantity_products,
                    purchase_orders.status = "APROBADO"
                    where purchases.id = NEW.id;        
            END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER update_product_stock');
    }
}
