<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_products', function (Blueprint $table) {
            $table->id();
            $table->integer('quantity_products');
            $table->integer('total_product');
            $table->foreignId('product_id')->constrained('products');
            $table->foreignId('purchase_order_id')->constrained('purchase_orders');
            $table->enum('status',['ACTIVO' , 'EN PROCESO', 'APROBADO'])->default('ACTIVO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_products');
    }
}
