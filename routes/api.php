<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\PurchaseOrderController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ListProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//login 
Route::post('/login', [LoginController::class, 'login']);


Route::group(['middleware' => 'auth:api'], function() { 
    Route::post('/logout', [LoginController::class, 'logout']);

    Route::group(['middleware' => 'verify.role'], function() { 
        //categories
        Route::post('/categories/create', [CategoryController::class, 'store']);
        Route::delete('/category-delete', [CategoryController::class, 'delete']);
    
        //users
        Route::get('/users', [UserController::class, 'index']);
        Route::post('/users/create', [UserController::class, 'store']);
        Route::delete('/user-delete', [UserController::class, 'destroy']);
        Route::put('/users/update', [UserController::class, 'update']);
  
        //products
        Route::post('/products/create', [ProductController::class, 'store']);
        Route::delete('/product-delete', [ProductController::class, 'destroy']);
        Route::put('/products/update', [ProductController::class, 'update']);
  
        //payment_methods
        Route::get('/payment-methods', [PaymentMethodController::class, 'index']);
        Route::post('/payment-methods/create', [PaymentMethodController::class, 'store']);
        Route::delete('/payment-method-delete', [PaymentMethodController::class, 'destroy']);
        Route::put('/payment-methods/update', [PaymentMethodController::class, 'update']);
        
    });
    
    //categories
    Route::get('/categories', [CategoryController::class, 'index']);
    
    
    //products
    Route::get('/products', [ProductController::class, 'index']);
    
    
    //settings
    Route::get('/settings', [SettingController::class, 'index']);
    Route::post('/settings/create', [SettingController::class, 'store']);
    Route::delete('/setting-delete', [SettingController::class, 'destroy']);
    Route::put('/settings/update', [SettingController::class, 'update']);
    
    //purchaseOrder
    Route::get('/purchase-orders', [PurchaseOrderController::class, 'index']);
    Route::post('/purchase-orders/create', [PurchaseOrderController::class, 'store']);
    Route::delete('/purchase-order-delete', [PurchaseOrderController::class, 'destroy']);
    Route::put('/purchase-orders/update', [PurchaseOrderController::class, 'update']);
    
    //ListProductController
    Route::post('/purchase-orders/add', [ListProductController::class, 'addToOrder']);
    
    //purchase
    Route::get('/purchase', [PurchaseController::class, 'index']);
    Route::post('/purchase/create', [PurchaseController::class, 'store']);
    Route::delete('/purchase-delete', [PurchaseController::class, 'destroy']);
    Route::put('/purchase/update', [PurchaseController::class, 'update']);
    
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
